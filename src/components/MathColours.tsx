import React, { ReactNode } from "react";
import { pixels } from "../constants";

interface Colour {
  red: number;
  green: number;
  blue: number;
}

/**
 *
 * @param n Given a number, give a unique colour based within the set of discrete colours
 * @returns Returns a rgb object with values between 8 and 256 in steps of 8
 */
const getMathColours = (n: number): Colour => {
  if (n >= 1 && n <= 32768) {
    const red = Math.ceil(n / 1024) * 8;

    const green = Math.ceil((n - Math.floor(n / 1024) * 1024) / 32) * 8;

    const value = (n % 32) * 8;
    const blue = value > 0 ? value : 256;
    return { red, green, blue };
  }

  console.error("Should be within value");
  return { red: 0, green: 0, blue: 0 };
};

export const MathColours = React.memo(() => {
  const nodes: ReactNode[] = [];

  for (let i = 1; i <= 32768; i++) {
    const { red, green, blue } = getMathColours(i);

    nodes.push(
      <div
        key={`rgb(${red},${green},${blue})`}
        style={{
          height: pixels,
          width: pixels,
          backgroundColor: `rgb(${red},${green},${blue})`,
          top: Math.floor(i / 256),
          left: Math.ceil(i - Math.floor(i / 256) * 256),
          position: "absolute",
        }}
      />
    );
  }
  return <>{nodes}</>;
});
