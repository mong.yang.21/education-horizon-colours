import React from "react";
import { pixels, step } from "../constants";

interface Point {
  backgroundColor: string;
  top: number;
  left: number;
}

/**
 * Use a brute force method that steps through every combination of colours
 * @returns Returns an object with the background colour and position
 */
export const getSimpleColours = () => {
  const grid: Point[] = [];

  let r = 0;
  let g = 0;
  let b = 0;

  let x = 0;
  let y = 0;

  while (r < 256 && g < 256 && b < 256) {
    let backgroundColor = `rgb(${r + 8},${g + 8},${b + 8})`;
    if (b < 256) {
      b += step;
    }

    if (b >= 256 && g < 256) {
      g += step;
      b = 0;
    }

    if (g >= 256 && r < 256) {
      r += step;
      g = 0;
    }

    if (x < 256) {
      x += 1;
    } else {
      x = 0;
      y += 1;
    }

    grid.push({
      backgroundColor,
      top: y,
      left: x,
    });

    if (r >= 256) break;
  }
  return grid;
};

export const SimpleColors = React.memo(() => {
  return (
    <>
      {getSimpleColours().map(({ backgroundColor, top, left }) => {
        return (
          <div
            key={backgroundColor}
            style={{
              height: pixels,
              width: pixels,
              backgroundColor,
              top,
              left,
              position: "absolute",
            }}
          />
        );
      })}
    </>
  );
});
