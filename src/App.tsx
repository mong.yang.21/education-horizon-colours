import { MathColours } from "./components/MathColours";

function App() {
  return <MathColours />;
}

export default App;
